package org.toromtomtom

import cats._
import cats.data._
import cats.implicits._
import org.toromtomtom.fpexp.rng.RNG

import scala.io.Source

package object fpexp {

  def fileContents(path: String): String = {
    val source = Source.fromFile(path)
    val contents = source.mkString
    source.close()
    contents
  }

  object StateF {
    def map[F[_]: Applicative, S, A, B](fa: State[S, F[A]])(
        f: A => B): State[S, F[B]] =
      stateApplicative[F, S].map(fa)(f)
  }

  implicit def stateApplicative[F[_]: Applicative, S]
    : Applicative[Lambda[x => State[S, F[x]]]] =
    Applicative[State[S, ?]].compose(Applicative[F])

  def toStateT[F[_]: Applicative, S, A](sfa: State[S, F[A]]): StateT[F, S, A] =
    StateT[F, S, A] { s =>
      val (s1, fa) = sfa.run(s).value
      fa.map((s1, _))
    }

  val randomLong: State[RNG, Long] = State[RNG, Long](_.nextLong)

  val randomDouble: State[RNG, Double] = State[RNG, Double](_.nextDouble)

  val randomInt: State[RNG, Int] = State[RNG, Int](_.nextInt)

  def randomIntUntil(upper: Int): State[RNG, Int] =
    State[RNG, Int](_.nextInt(upper))

  case class Interval(lower: BigDecimal, upper: BigDecimal)

  def evalTo[F[_]](implicit app: Applicative[F]): Eval ~> F = new (Eval ~> F) {
    override def apply[A](fa: Eval[A]): F[A] = app.pure(fa.value)
  }
}
