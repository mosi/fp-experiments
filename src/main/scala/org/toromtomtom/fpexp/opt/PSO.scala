package org.toromtomtom.fpexp.opt

import cats._
import cats.data._
import cats.implicits._
import org.toromtomtom.fpexp._
import org.toromtomtom.fpexp.rng.RNG

object PSO {

  def fromInterval(interval: Interval): State[RNG, Double] = {
    randomDouble
      .map(d => d * (interval.upper - interval.lower) + interval.lower)
      .map(_.toDouble)
  }

  def fromIntervals(intervals: Vector[Interval]): State[RNG, Vector[Double]] =
    intervals.traverse(fromInterval)

  case class Config(omega: Double, thetaP: Double, thetaG: Double)

  case class Particle(pos: Vector[Double],
                      velo: Vector[Double],
                      localBest: Vector[Double],
                      localTarget: Double,
                      globalBest: Vector[Double],
                      globalTarget: Double)

  def init[F[_]: Monad](popSize: Int,
                        intervals: Vector[Interval],
                        target: Vector[Double] => StateT[F, RNG, Double])
    : StateT[F, RNG, Vector[Particle]] = {

    def initOne: StateT[F, RNG, Particle] =
      for {
        pos <- fromIntervals(intervals).mapK(evalTo[F])
        velo <- fromIntervals(intervals.map {
          case Interval(l, u) => Interval(-(u - l).abs, (u - l).abs)
        }).mapK(evalTo[F])
        gt <- target(pos)
      } yield Particle(pos, velo, pos, gt, pos, gt)

    Vector.fill(popSize)(initOne).sequence
  }

  def step[F[_]: Monad](particles: Vector[Particle],
                        target: Vector[Double] => StateT[F, RNG, Double],
                        config: Config): StateT[F, RNG, Vector[Particle]] = {

    val (globalBest, globalTarget) = {
      val p = particles.minBy(_.globalTarget)
      (p.globalBest, p.globalTarget)
    }

    def stepOne(particle: Particle): StateT[F, RNG, Particle] = {
      val Particle(pos, velo, localBest, localTarget, _, _) = particle

      val newPosAndVelo: Vector[State[RNG, (Double, Double)]] =
        for {
          Vector(p, v, l, g) <- Vector(pos, velo, localBest, globalBest).transpose
        } yield
          (randomDouble, randomDouble).mapN {
            case (rp, rg) =>
              val nv = config.omega * v +
                config.thetaP * rp * (l - p) +
                config.thetaG * rg * (g - p)
              val np = p + nv
              (np, nv)
          }

      for {
        pv <- newPosAndVelo.sequence.mapK(evalTo[F])
        (np, nv) = pv.unzip
        tnp <- target(np)
        (nlb, nlt) = if (tnp < localTarget) (np, tnp)
        else (localBest, localTarget)
        (ngb, ngt) = if (tnp < globalTarget) (np, tnp)
        else (globalBest, globalTarget)
      } yield Particle(np, nv, nlb, nlt, ngb, ngt)
    }

    particles.map(stepOne).sequence
  }

  def minimize[F[_]: Monad](
      popSize: Int,
      intervals: Vector[Interval],
      iterations: Int,
      target: Vector[Double] => StateT[F, RNG, Double],
      config: Config): StateT[F, RNG, (Vector[Double], Double)] = {

    val initPop = init(popSize, intervals, target)

    val finalPop = (0 until iterations).foldLeft(initPop) {
      case (pop, _) => pop.flatMap(particles => step(particles, target, config))
    }

    finalPop.map(particles =>
      (particles.head.globalBest, particles.head.globalTarget))
  }
}
