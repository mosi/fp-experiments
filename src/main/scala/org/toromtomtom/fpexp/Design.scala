package org.toromtomtom.fpexp

import cats._
import cats.data._
import cats.implicits._
import org.toromtomtom.fpexp.rng.RNG

object Design {

  def runDesign[F[_]: Monad, P, S, A](
      design: List[Map[String, P]]
  )(run: Map[String, P] => StateT[F, S, A])
    : StateT[F, S, List[(Map[String, P], A)]] =
    design.traverse(params => run(params).map(params -> _))

  def fullFactorial[P](
      factors: Map[String, Iterable[P]]): List[Map[String, P]] = {

    @annotation.tailrec
    def go(fs: List[(String, Iterable[P])],
           acc: List[Map[String, P]]): List[Map[String, P]] =
      fs match {
        case Nil => acc
        case (name, values) :: rest =>
          go(rest, acc.map2(values.toList)((map, v) => map.updated(name, v)))
      }

    go(factors.toList, List(Map.empty))
  }

  def cross[P](des1: List[Map[String, P]],
               des2: List[Map[String, P]]): List[Map[String, P]] =
    for {
      point1 <- des1
      point2 <- des2
    } yield point1 ++ point2

  def latinHypercube(factors: Map[String, Interval],
                     np: Int): State[RNG, List[Map[String, BigDecimal]]] = {

    val randomPerm: State[RNG, Vector[Int]] = {
      def swap(as: Vector[Int], i: Int, j: Int): Vector[Int] =
        as.updated(i, as(j)).updated(j, as(i))

      def step(as: Vector[Int], i: Int): State[RNG, Vector[Int]] =
        for {
          offset <- randomIntUntil(as.length - i)
          j = i + offset
        } yield swap(as, i, j)

      (0 to np - 2)
        .foldLeft(State.pure[RNG, Vector[Int]]((0 until np).toVector))((s, i) =>
          s.flatMap(vec => step(vec, i)))
    }

    def oneFactor(interval: Interval) =
      randomPerm.map(vec =>
        vec.map(i =>
          (interval.upper - interval.lower) * i / (np - 1) + interval.lower))

    val allFactors = factors.toList.map {
      case (p, i) => oneFactor(i).map(vs => vs.map(v => (p, v)))
    }

    val allConfigs = allFactors.sequence.map(_.transpose)

    allConfigs.map(_.map(_.toMap))

  }

}
