package org.toromtomtom.fpexp

import cats.Show
import cats.implicits._

object Observation {

  type Trajectory[T] = Map[Double, T]

  type Trajectories[Observable[_], T] = Map[Observable[T], Trajectory[T]]

  def addObservations[Observable[_], T](
      time: Double,
      observables: List[Observable[T]],
      previousObservations: Trajectories[Observable, T]
  )(observe: Observable[T] => T): Map[Observable[T], Trajectory[T]] =
    observables.foldLeft(previousObservations) { (prevObs, observable) =>
      val previousTrajectory = prevObs.getOrElse(observable, Map.empty)
      prevObs.updated(observable,
                      previousTrajectory.updated(time, observe(observable)))
    }

  def transpose[Observable[_], T](ts: List[Trajectories[Observable, T]])
    : Map[Observable[T], List[Trajectory[T]]] = {
    val all = for {
      run <- ts
      (obs, trajectory) <- run
    } yield (obs, trajectory)
    all.groupBy(_._1).mapValues(s => s.map(_._2))
  }

  def sort[T](t: Trajectory[T]): List[(Double, T)] = t.toList.sortBy(_._1)

  def format[Observable[_], T](
      data: List[(Map[String, Any], List[Trajectories[Observable, T]])]
  )(implicit obsShow: Show[Observable[T]]): String = {

    def formatRun(run: Trajectories[Observable, T]): String =
      run
        .map {
          case (obs, traj) => obs.show + "\n" + sort(traj).mkString("\n") + "\n"
        }
        .mkString("\n")

    val paramBlocks = for {
      (params, runs) <- data
    } yield {
      params.mkString(", ") + "\n" + runs.map(formatRun).mkString("\n")
    }

    "\n" + paramBlocks.mkString("\n" + "-" * 20 + "\n")
  }
}
