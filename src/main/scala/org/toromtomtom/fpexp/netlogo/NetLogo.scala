package org.toromtomtom.fpexp.netlogo

import cats.Show
import org.nlogo.headless.HeadlessWorkspace
import org.toromtomtom.fpexp.Observation._

object NetLogo {

  case class NLExpression[T](exp: String, cast: String => T)
      extends (HeadlessWorkspace => T) {
    override def toString(): String = exp

    override def apply(ws: HeadlessWorkspace): T = cast(ws.report(exp).toString)
  }

  implicit def showNLExpression[T]: Show[NLExpression[T]] =
    new Show[NLExpression[T]] {
      override def show(t: NLExpression[T]): String = t.exp
    }

  def run[T](model: String,
             stopCond: NLExpression[Boolean],
             observables: List[NLExpression[T]],
             params: Map[String, Any],
             seed: Int): Map[NLExpression[T], Trajectory[T]] = {

    // println(s"starting a run with seed $seed and params $params")

    val ws = HeadlessWorkspace.newInstance

    try {
      ws.openString(model)
      for ((param, value) <- params)
        ws.command(s"set $param $value")
      ws.command(s"random-seed $seed")
      ws.command("setup")

      val simStream = {
        Stream.iterate((0.0, false, Map.empty[NLExpression[T], Map[Double, T]])) {
          case (t, _, prevObs) =>
            val updatedObs =
              addObservations(t, observables, prevObs)(o => o(ws))
            ws.command("go")
            val nt = nlDouble("ticks")(ws)
            val shouldStop = (t == nt) || stopCond(ws)
            (nt, shouldStop, updatedObs)
        }
      }

      simStream.dropWhile { case (_, shouldStop, _) => !shouldStop }.head._3
    } finally {
      ws.dispose()
    }
  }

  def nlDouble(s: String): NLExpression[Double] = NLExpression(s, _.toDouble)

  def nlBoolean(s: String): NLExpression[Boolean] = NLExpression(s, _.toBoolean)

}
