package org.toromtomtom.fpexp.smc

import cats.data.{State, StateT}
import cats.effect.IO
import org.toromtomtom.fpexp.StateF

object SRPT {
  def check[S, A](theRun: State[S, IO[A]],
                  batchSize: Int,
                  property: A => Boolean,
                  p: Double,
                  alpha: Double,
                  beta: Double,
                  delta: Double): StateT[IO, S, Boolean] = {

    // see Legay et. al: Statistical Model Checking: An Overview, section 3.1, page 5
    //
    // the method is based on a quotient p1m/p0m that is computed after a certain number of runs
    // have been executed and the property has been checked on the results
    // if p1m/p0m >= a, the property is satisfied
    // if p1m/p0m <= b, the property is not satisfied
    // else, more runs are needed to come to a conclusion
    //
    // the algorithm is guaranteed to terminate

    val a = (1 - beta) / alpha
    val b = beta / (1 - alpha)
    val p0 = p + delta
    val p1 = p - delta

    def quotient(satisfied: Int, total: Int): Double = {
      val p1m = math.pow(p1, satisfied) * math.pow(1 - p1, total - satisfied)
      val p0m = math.pow(p0, satisfied) * math.pow(1 - p0, total - satisfied)
      p1m / p0m
    }

    def result(completed: List[Boolean]): Option[Boolean] = {
      val satisfied = completed.count(_ == true)
      val total = completed.size
      val q = quotient(satisfied, total)
      if (q >= a) Some(false)
      else if (q <= b) Some(true)
      else None
    }

    def enoughRuns(completed: List[Boolean]): Boolean =
      result(completed).isDefined

    import org.toromtomtom.fpexp.AsyncDesign._

    val checkedRun = StateF.map(theRun)(property)

    val batch = parametrization(batchSize, checkedRun)

    val runs = until(batch, enoughRuns)

    runs.map(result).map(_.get)
  }
}
