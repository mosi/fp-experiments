package org.toromtomtom.fpexp.smc

import cats.data._
import cats.effect.IO
import org.apache.commons.math3.distribution.BinomialDistribution
import org.toromtomtom.fpexp._

object SSP {

  def check[S, A](theRun: State[S, IO[A]],
                  property: A => Boolean,
                  p: Double,
                  alpha: Double,
                  beta: Double,
                  delta: Double): StateT[IO, S, Boolean] = {

    val replications: Int = {
      // see Sen et.al: On Statistical Model Checking of Stochastic Systems, section 3.1
      //
      // assume that the number of satisfying runs Y is binomially distributed
      // compute an n for which the following two inequalities hold:
      // Prob[Y/n >= p] = Prob[Y >= p * n] = 1 - Prob[Y < p * n] <= alpha when Y ~ B(n, p - delta)
      // Prob[Y/n < p] = Prob[Y < p * n]<= beta when Y ~ B(n, p + delta)
      // start with some small n and increase it until both inequalities hold

      var n = 1
      var done = false

      do {
        n += 1
        val probGreater = 1 - new BinomialDistribution(
          n,
          math.max(p - delta, 0)
        ).cumulativeProbability((p * n).toInt)
        val probLesser = new BinomialDistribution(n, math.min(p + delta, 1))
          .cumulativeProbability((p * n).toInt)
        done = probGreater <= alpha && probLesser <= beta
      } while (!done)

      n
    }

    import AsyncDesign._

    val checkedRun = StateF.map(theRun)(property)

    val all = parametrization(replications, checkedRun)
    val successRatio =
      StateF.map(all)(runs => runs.count(identity).toDouble / runs.size)
    toStateT(StateF.map(successRatio)(r => r >= p))
  }

}
