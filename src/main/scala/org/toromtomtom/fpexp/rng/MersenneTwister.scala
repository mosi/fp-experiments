package org.toromtomtom.fpexp.rng

import org.apache.commons.rng.simple.RandomSource
import org.apache.commons.rng.{RandomProviderState, UniformRandomProvider}

case class MersenneTwister(private val state: RandomProviderState) extends RNG {

  private def queryRNG[A](f: UniformRandomProvider => A): (RNG, A) = {
    val rng = RandomSource.create(RandomSource.MT_64)
    rng.restoreState(state)
    val a = f(rng)
    val s = rng.saveState()
    (MersenneTwister(s), a)
  }

  override def nextLong: (RNG, Long) = queryRNG(_.nextLong())

  override def nextInt: (RNG, Int) = queryRNG(_.nextInt())

  override def nextDouble: (RNG, Double) = queryRNG(_.nextDouble())

  override def nextInt(upper: Int): (RNG, Int) = queryRNG(_.nextInt(upper))
}

object MersenneTwister {
  def apply(seed: Long): MersenneTwister = {
    val rng = RandomSource.create(RandomSource.MT_64, seed)
    val s = rng.saveState()
    MersenneTwister(s)
  }
}
