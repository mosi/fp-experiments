package org.toromtomtom.fpexp.rng

trait RNG {
  def nextLong: (RNG, Long)

  // naive implementation, override if possible
  def nextInt: (RNG, Int) = {
    val (rng1, l) = nextLong
    (rng1, l.toInt)
  }

  // naive implementation, override if possible
  def nextDouble: (RNG, Double) = {
    val (rng1, l) = nextLong
    val d = (l.toDouble - Long.MinValue.toDouble) / (Long.MaxValue.toDouble - Long.MinValue.toDouble)
    (rng1, d)
  }

  // naive implementation, override if possible
  def nextInt(upper: Int): (RNG, Int) = {
    val (rng1, d) = nextDouble
    (rng1, (d * upper).toInt)
  }
}
