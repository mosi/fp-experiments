package org.toromtomtom.fpexp

import java.io.Writer
import java.nio.file.{FileSystems, Files, Path}

import cats.Show
import cats.effect.Async
import org.apache.commons.csv.CSVFormat
import org.toromtomtom.fpexp.Observation._

object Storage {

  def writeCSV[F[_]: Async, Obs[_], A: Show](
      path: String
  )(results: List[List[Trajectories[Obs, A]]])(
      implicit showObs: Show[Obs[A]]): F[Unit] = {

    val rootDir: F[Path] = dir(path)

    def createPointDir(parent: Path, index: Int): F[Path] =
      dir(parent.resolve(s"point-$index").toAbsolutePath.toString)

    def createRunFile(parent: Path,
                      trajs: Trajectories[Obs, A],
                      index: Int): F[Unit] =
      writeCSVFile(parent, s"run-$index")(trajs)

    import cats.implicits._

    val points = (for {
      (trajs, idx) <- results.zipWithIndex
    } yield
      rootDir.flatMap(
        root =>
          createPointDir(root, idx).flatMap(
            pointDir =>
              (for {
                (traj, idx2) <- trajs.zipWithIndex
              } yield createRunFile(pointDir, traj, idx2)).sequence
        )
      )).sequence

    points.map(_ => ())
  }

  def writeCSVFile[F[_]: Async, Obs[_], A: Show](path: Path, fileName: String)(
      trajectories: Trajectories[Obs, A]
  )(implicit showObs: Show[Obs[A]]): F[Unit] =
    Async[F].bracket(Async[F].pure {
      val parent = Files.createDirectories(path)
      Files.newBufferedWriter(parent.resolve(fileName))
    })(writeDataToCSV[F, Obs, A](trajectories))(writer =>
      Async[F].pure(writer.close()))

  def dir[F[_]: Async](root: String): F[Path] = Async[F].pure {
    val path = FileSystems.getDefault.getPath(root)
    Files.createDirectories(path)
    path
  }

  def linesFromTrajectories[Obs[_], A: Show](
      trajectories: Trajectories[Obs, A],
      observables: List[Obs[A]]): Seq[Seq[String]] = {

    val observations = for {
      (obs, traj) <- trajectories
      (t, a) <- traj
    } yield (t, obs, a)

    val grouped = observations.groupBy(_._1).toList.sortBy(_._1)

    val lines = grouped.map {
      case (t, o) =>
        val values = o.map(triple => (triple._2, triple._3)).toMap
        t.toString :: observables.map(values).map(Show[A].show)
    }

    lines
  }

  def writeDataToCSV[F[_]: Async, Obs[_], A: Show](
      trajectories: Trajectories[Obs, A]
  )(writer: Writer)(implicit showObs: Show[Obs[A]]): F[Unit] = Async[F].pure {

    val observables = trajectories.keys.toList

    val header: List[String] = "time" :: observables.map(showObs.show)

    val csvPrinter = CSVFormat.DEFAULT.withHeader(header: _*).print(writer)

    val lines = linesFromTrajectories(trajectories, observables)

    for (line <- lines)
      csvPrinter.printRecord(line.toArray.asInstanceOf[Array[Object]]: _*)

  }

}
