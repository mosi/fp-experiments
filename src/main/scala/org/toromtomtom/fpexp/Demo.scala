package org.toromtomtom.fpexp

import cats._
import cats.data._
import cats.effect._
import cats.implicits._
import com.typesafe.scalalogging.Logger
import org.toromtomtom.fpexp.Design._
import org.toromtomtom.fpexp.Observation.Trajectory
import org.toromtomtom.fpexp.netlogo.NetLogo
import org.toromtomtom.fpexp.netlogo.NetLogo.{NLExpression, _}
import org.toromtomtom.fpexp.opt.PSO.Config
import org.toromtomtom.fpexp.rng.{MersenneTwister, RNG}
import org.toromtomtom.fpexp.smc.SRPT

object Sequence {

  // sequential
  def sequenceM[F[_]: Monad, S, A](
      tas: List[StateT[F, S, A]]): StateT[F, S, List[A]] =
//    tas.sequence[StateT[F, S, ?], A]
    tas.sequence[Lambda[x => StateT[F, S, x]], A]

  // parallel
  def sequenceA[F[_]: Applicative, S, A](
      tas: List[State[S, F[A]]]): State[S, F[List[A]]] =
    tas.sequence[Lambda[x => State[S, F[x]]], A]

}

object ExampleModel {
  val contents: String = fileContents("src/main/resources/Traffic Basic.nlogo")
}

object SimpleDesign {

  import Observation._
  import netlogo.NetLogo._

  def run[S, A, B](randomA: State[S, A])(run: A => B): State[S, B] =
    // more succinct: randomA.map(run)
    randomA.map(seed => run(seed))

  def parametrization[S, A](n: Int, run: State[S, A]): State[S, List[A]] =
    run.replicateA(n)

  def experiment[S, P, A](params: List[P])(
      parametrization: P => State[S, List[A]]): State[S, List[List[A]]] =
    params.traverse(parametrization)

  val fixedNumRunsExperiment
    : State[RNG, List[List[Map[NLExpression[Double], Trajectory[Double]]]]] =
    experiment(
      params = List(Map("acceleration" -> 0.01, "deceleration" -> 0.01),
                    Map("acceleration" -> 0.01, "deceleration" -> 0.05)))(
      p =>
        parametrization(
          n = 5,
          run = run(randomInt)(
            s =>
              NetLogo.run(model = ExampleModel.contents,
                          stopCond = nlBoolean("ticks > 50"),
                          observables = List(nlDouble("[speed] of sample-car")),
                          params = p,
                          seed = s))
      )
    )

  val inlinedExperiment
    : State[RNG, List[List[Map[NLExpression[Double], Trajectory[Double]]]]] = {
    val params = List(Map("acceleration" -> 0.01, "deceleration" -> 0.01),
                      Map("acceleration" -> 0.01, "deceleration" -> 0.05))
    params.traverse { p =>
      randomInt
        .map { seed =>
          NetLogo.run(model = ExampleModel.contents,
                      stopCond = nlBoolean("ticks > 50"),
                      observables = List(nlDouble("[speed] of sample-car")),
                      p,
                      seed)
        }
        .replicateA(5)
    }
  }

  def main(args: Array[String]): Unit = {
    val results = fixedNumRunsExperiment.runA(MersenneTwister(42)).value
    println(results)
  }
}

object AsyncDesign extends IOApp {

  import Observation._
  import netlogo.NetLogo._

  def nlRun[A](model: String,
               stopCond: NLExpression[Boolean],
               observables: List[NLExpression[A]],
               params: Map[String, Any],
               seed: Int): Map[NLExpression[A], Trajectory[A]] =
    NetLogo.run(model, stopCond, observables, params, seed)

  def run[S, A, B](randomA: State[S, A])(run: A => B): State[S, IO[B]] =
    randomA.map(seed => IO.delay(run(seed)))

  def parametrization[S, A](n: Int,
                            run: State[S, IO[A]]): State[S, IO[List[A]]] =
    run.replicateA(n).map(fs => fs.parSequence)

  def experiment[S, A](
      params: List[Map[String, Any]],
      parametrization: Map[String, Any] => State[S, IO[List[A]]])
    : State[S, IO[List[List[A]]]] =
    params.traverse(parametrization).map(fs => fs.parSequence)

  val fixedNumRunsExperimentF
    : State[RNG,
            IO[List[List[Map[NLExpression[Double], Trajectory[Double]]]]]] =
    experiment(
      params = List(Map("acceleration" -> 0.01, "deceleration" -> 0.01),
                    Map("acceleration" -> 0.01, "deceleration" -> 0.05)),
      p =>
        parametrization(
          n = 5,
          run(randomInt)(
            seed =>
              nlRun(model = ExampleModel.contents,
                    stopCond = nlBoolean("ticks > 50"),
                    observables = List(nlDouble("[speed] of sample-car")),
                    p,
                    seed))
      )
    )

  def until[S, A](batch: State[S, IO[List[A]]],
                  replCond: List[A] => Boolean): StateT[IO, S, List[A]] = {

    def go(runs: StateT[IO, S, List[A]]): StateT[IO, S, List[A]] =
      for {
        doneRuns <- runs
        result <- if (replCond(doneRuns)) doneRuns.pure[StateT[IO, S, ?]]
        else {
          val moreRuns = for {
            newRuns <- toStateT(batch)
          } yield doneRuns ++ newRuns
          go(moreRuns)
        }
      } yield result

    go(List.empty[A].pure[StateT[IO, S, ?]])
  }

  def robustBatch[S, A](n: Int, run: State[S, IO[A]]): State[S, IO[List[A]]] = {
    val eithers =
      run.map(Async[IO].attempt).replicateA(n).map(fs => fs.parSequence)
    StateF.map(eithers) { es =>
      es.collect {
        // logging as a side effect
        case Left(t) =>
          Logger("MyExperiment").warn("Throwing away failed run", t)
      }
      es.collect {
        case Right(r) => r
      }
    }
  }

  override def run(
      args: List[String]
  ): IO[ExitCode] = {
    fixedNumRunsExperimentF
      .runA(MersenneTwister(42))
      .value
      .flatMap(Storage.writeCSV[IO, NLExpression, Double]("results"))
      .as(ExitCode.Success)
  }

}

object LatinHypercube extends IOApp {

  import AsyncDesign._

  def designPoint(p: Map[String, Any])
    : State[RNG, IO[List[Map[NLExpression[Double], Trajectory[Double]]]]] =
    parametrization(
      n = 5,
      run = AsyncDesign.run(randomInt)(
        seed =>
          nlRun(model = ExampleModel.contents,
                stopCond = nlBoolean("ticks > 50"),
                observables = List(nlDouble("[speed] of sample-car")),
                p,
                seed)
      )
    )

  val experiment
    : State[RNG,
            IO[List[List[Map[NLExpression[Double], Trajectory[Double]]]]]] =
    for {
      design <- latinHypercube(Map("acceleration" -> Interval(0.01, 0.08),
                                   "deceleration" -> Interval(0.01, 0.08)),
                               10)
      allRuns <- design.traverse(designPoint)
    } yield allRuns.sequence

  override def run(args: List[String]): IO[ExitCode] =
    experiment
      .runA(MersenneTwister(42))
      .value
      .map(println)
      .as(ExitCode.Success)
}

object Optimization extends IOApp {

  import AsyncDesign._

  val optimizationExperiment: StateT[IO, RNG, (Vector[Double], Double)] = {

    val searchSpace = Map("acceleration" -> Interval(0, 0.01),
                          "deceleration" -> Interval(0, 0.1))

    val observable = nlDouble("[speed] of sample-car")

    val paramNames = searchSpace.keys.toVector.sorted

    def target(ds: Vector[Double]): StateT[IO, RNG, Double] = {
      val params = paramNames.zip(ds).toMap
      val randomRun = parametrization(
        n = 4,
        AsyncDesign.run(randomInt)(
          seed =>
            nlRun(model = ExampleModel.contents,
                  stopCond = nlBoolean("ticks > 50"),
                  observables = List(observable),
                  params,
                  seed)
        )
      )
      val speeds = toStateT(randomRun).map(results =>
        results.flatMap(_(observable).values.toList))
      speeds.map(s => -s.count(_ == 0).toDouble / s.size)
    }

    opt.PSO.minimize(popSize = 4,
                     intervals = paramNames.map(searchSpace),
                     iterations = 10,
                     target = target,
                     config = Config(0.01, 0.1, 0.1))

  }

  override def run(args: List[String]): IO[ExitCode] =
    optimizationExperiment
      .runA(MersenneTwister(42))
      .map(println)
      .as(ExitCode.Success)

}

object StatisticalModelChecking extends IOApp {

  import AsyncDesign._

  val obs = nlDouble("[speed] of sample-car")

  def redCarNeverStops(
      results: Map[NLExpression[Double], Trajectory[Double]]): Boolean =
    !results(obs).values.toList.contains(0.0)

  override def run(args: List[String]): IO[ExitCode] =
    SRPT
      .check(
        AsyncDesign.run(randomInt)(
          seed =>
            nlRun(model = ExampleModel.contents,
                  stopCond = nlBoolean("ticks > 50"),
                  observables = List(obs),
                  params = Map("acceleration" -> 0.01, "deceleration" -> 0.01),
                  seed)
        ),
        batchSize = 4,
        redCarNeverStops,
        p = 0.8,
        alpha = 0.05,
        beta = 0.05,
        delta = 0.05
      )
      .runA(MersenneTwister(42))
      .map(println)
      .as(ExitCode.Success)
}

object pureIOExperiment extends IOApp {

  private def runExperiment(model: String) =
    AsyncDesign.experiment(
      params = List(Map("acceleration" -> 0.01, "deceleration" -> 0.01),
                    Map("acceleration" -> 0.01, "deceleration" -> 0.05)),
      p =>
        AsyncDesign.parametrization(
          n = 5,
          AsyncDesign.run(randomInt)(
            seed =>
              AsyncDesign.nlRun(model = model,
                                stopCond = nlBoolean("ticks > 50"),
                                observables =
                                  List(nlDouble("[speed] of sample-car")),
                                p,
                                seed))
      )
    )

  import NetLogo._

  override def run(args: List[String]): IO[ExitCode] =
    for {
      model <- IO(fileContents("src/main/resources/Traffic Basic.nlogo"))
      results <- runExperiment(model).runA(MersenneTwister(42)).value
      _ <- Storage.writeCSV[IO, NLExpression, Double]("results")(results)
    } yield ExitCode.Success
}

object PerformanceBenchmarks {
  val params = fullFactorial(
    Map("acceleration" -> Range.BigDecimal(0.00, 0.10, 0.05),
        "deceleration" -> Range.BigDecimal(0.00, 0.10, 0.05)))

  val runsPerParam = 3
}

class PerformanceBenchmarks {

  import PerformanceBenchmarks._

  import org.openjdk.jmh.annotations.Benchmark

  import scala.collection.mutable
  import scala.concurrent.duration.Duration
  import scala.concurrent.{Await, ExecutionContext, Future}
  import scala.util.Random

  @Benchmark
  def imperativeExperiment = {

    val random = new Random()
    val results =
      mutable.ListBuffer.fill(params.size)(
        List.empty[Map[NLExpression[Double], Trajectory[Double]]])

    implicit val ec = ExecutionContext.global

    val futures = for (p <- params.indices; _ <- 0 until runsPerParam) yield {

      val seed = random.nextInt()

      Future {
        val result = NetLogo.run(model = ExampleModel.contents,
                                 stopCond = nlBoolean("ticks > 50"),
                                 observables =
                                   List(nlDouble("[speed] of sample-car")),
                                 params(p),
                                 seed)

        results.synchronized {
          val resultsForP = results(p)
          results(p) = result :: resultsForP
        }
      }
    }

    Await.ready(Future.sequence(futures), Duration.Inf)

    results
  }

  @Benchmark
  def functionalExperiment = {
    val future = AsyncDesign
      .experiment(
        params,
        p =>
          AsyncDesign.parametrization(
            runsPerParam,
            AsyncDesign.run(randomInt)(
              seed =>
                NetLogo.run(model = ExampleModel.contents,
                            stopCond = nlBoolean("ticks > 50"),
                            observables =
                              List(nlDouble("[speed] of sample-car")),
                            p,
                            seed))
        )
      )
      .runA(MersenneTwister(42))
      .value
      .unsafeToFuture()

    Await.result(future, Duration.Inf)
  }

}
