name := "fp-experiments"

version := "0.1"

scalaVersion := "2.12.8"

resolvers += "NetLogo BinTray" at "https://dl.bintray.com/netlogo/NetLogo-JVM"

libraryDependencies ++= Seq("org.typelevel" %% "cats-core" % "1.5.0",
                            "org.typelevel" %% "cats-effect" % "1.2.0",
                            "org.nlogo" % "netlogo" % "6.0.4",
                            "org.apache.commons" % "commons-csv" % "1.6",
                            "org.apache.commons" % "commons-math3" % "3.6.1",
                            "org.apache.commons" % "commons-rng-client-api" % "1.2",
                            "org.apache.commons" % "commons-rng-simple" % "1.2",
                            "ch.qos.logback" % "logback-classic" % "1.2.3", 
                            "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2"
)

addCompilerPlugin("org.spire-math" %% "kind-projector" % "0.9.9")
enablePlugins(JmhPlugin)

scalacOptions ++= Seq("-encoding",
                      "utf-8",
                      "-feature",
                      "-deprecation",
                      "-unchecked",
                      "-explaintypes",
                      "-language:implicitConversions",
                      "-language:higherKinds",
                      "-Ypartial-unification")
