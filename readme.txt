To compile and run, install sbt (https://www.scala-sbt.org/).
Start sbt in the project root and execute one of the following commands:

- runMain org.toromtomtom.fpexp.SimpleDesign

- runMain org.toromtomtom.fpexp.AsyncDesign

- runMain org.toromtomtom.fpexp.LatinHypercube

- runMain org.toromtomtom.fpexp.Optimization

- runMain org.toromtomtom.fpexp.StatisticalModelChecking

If you get OurOfMemoryErrors, try starting sbt with the increased memory: 

sbt -mem 2048
